# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Location(models.Model):
    name = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    pin_code = models.IntegerField()

    def __str__(self):
        return '%s ' % (self.name)


class AgentModel(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    company_name = models.CharField(max_length=255)
    location = models.ForeignKey(Location,null=True,blank=True)

    def __str__(self):
        return '%s - %s' % (self.first_name, self.last_name)


class Address(models.Model):
    address_line1 = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    pin_code = models.IntegerField()
    agent = models.ForeignKey(AgentModel, null=True,blank=True)

    def __str__(self):
        return '%s - %s' % (self.agent.first_name,self.address_line1)


class Media(models.Model):
    media_name = models.CharField(max_length=100)
    media_url = models.CharField(max_length=100)
    agent = models.ForeignKey(AgentModel,null=True,blank=True)
    def __str__(self):
        return '%s - %s' % (self.media_name, self.agent.first_name)



