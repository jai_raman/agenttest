from django import forms
from models import *



class LocationForm(forms.ModelForm):
    class Meta:
        model = Location
        exclude = []

class AgentForm(forms.ModelForm):
    class Meta:
        model = AgentModel
        exclude =[]

class AdressForm(forms.ModelForm):
    class Meta:
        model = Address
        exclude = ['agent']

class MediaForm(forms.ModelForm):
    class Meta:
        model = Media
        exclude = ['agent']