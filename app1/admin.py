# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import *
# Register your models here.
#registering to show in django admin
admin.site.register(Location)
admin.site.register(AgentModel)
admin.site.register(Address)
admin.site.register(Media)