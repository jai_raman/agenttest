# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.

from django.shortcuts import render_to_response
from django.http import HttpResponse,HttpResponseRedirect
from .forms import *
from django.views.generic.list import ListView
from django.views.decorators.csrf import csrf_exempt


#disabling csrf (cross site request forgery)
@csrf_exempt
def create_agent(request):
    if request.method == "POST":
        form = AgentForm(request.POST)
        #initializing the object of agent
        if form.is_valid():
            form.save()
            data_list = {}
            for i in request.POST:
                #dumping request.POST details into another dictionary
                data_list.update({i:request.POST[i]})
            model_ob = form.instance
            #getting agent object and creating address and media object
            address = Address.objects.create(address_line1=data_list['address_line1'],city=data_list['city'],state=data_list['state'],pin_code=data_list['pin_code'],agent=model_ob)
            media_ob = Media.objects.create(media_name=data_list['media_name'],media_url=data_list['media_url'],agent = model_ob)
    return render(request,'create_agent.html',{'form':AgentForm(),'form2':AdressForm(),'form3':MediaForm()})


#handling create location form
@csrf_exempt
def create_location(request):
    if request.method == "POST":
        form = LocationForm(request.POST)
        if form.is_valid():
            form.save()
    return render(request, 'create_location.html', {'location_form': LocationForm()})

#getting all the agents
class AgentListView(ListView):
    template_name = 'get_all_agents.html'
    model = AgentModel
    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(AgentListView, self).dispatch(request, *args, **kwargs)

